module ApplicationHelper
  def navbar_type
    case params[:controller]
    when 'blogs'
      'blogs'
    else
      'home'
    end
  end
end
