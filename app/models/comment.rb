class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :post

  scope :newest, -> { order('created_at DESC') }
end
