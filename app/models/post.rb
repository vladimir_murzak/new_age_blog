class Post < ActiveRecord::Base
  belongs_to :blog
  has_many :comments, dependent: :destroy

  scope :newest, -> { order('created_at DESC').limit(15) }
end
