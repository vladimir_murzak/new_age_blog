class Blog < ActiveRecord::Base

  has_attached_file :blog_image, styles: { medium: "300x300>", thumb: "50x50>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :blog_image, content_type: /\Aimage\/.*\Z/

  belongs_to :user
  has_many :posts, dependent: :destroy
end
