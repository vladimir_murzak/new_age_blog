class BlogsController < ApplicationController

  def my
    @blogs = current_user.blogs
  end

  def create
    binding.pry
    blog = current_user.blogs.create(blog_params)
    if blog.errors.blank?
      flash[:notice] = 'Post created successfully'
      redirect_to blog_path(blog)
    else
      flash[:error] = blog.errors.full_messages
      render :new
    end
  end

  def index
    @blogs = Blog.all
  end

  def show
    @blog = Blog.find(params[:id])
  end

  private

  def blog_params
    params.require(:blog).permit(:title, :desc, :blog_image)
  end
end
