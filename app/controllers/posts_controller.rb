class PostsController < ApplicationController
  before_action :get_post, only: [ :show ]

  respond_to :js, :html, only: :destroy

  def my
    @posts = current_user.posts
  end

  def create
    post = current_user.posts.create(post_params)
    if post.errors.blank?
      flash[:notice] = 'Post created successfully'
      redirect_to post_path(post)
    else
      flash[:error] = post.errors.full_messages
      render :new
    end
  end

  def destroy
    @post = Post.find(params[:id])
    user = @post.user
    @post.delete
    @posts = user.posts
    respond_with @post
  end

  private

  def post_params
    params[:post].permit!
  end

  def get_post
    @post = Post.find(params[:id])
  end
end
