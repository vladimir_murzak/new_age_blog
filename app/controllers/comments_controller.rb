class CommentsController < ApplicationController

  respond_to :js, :html, only: :destroy

  def create
    comment = current_user.comments.create(comment_params)
    if comment.errors.blank?
      flash[:notice] = 'Post created successfully'
    else
      flash[:error] = comment.errors.full_messages
    end
    redirect_to post_path(comment_params[:post_id])
  end

  def destroy
    @comment = Comment.find(params[:id])
    post = @comment.post
    @comment.delete
    @comments = post.comments.newest
    respond_with @comment
  end

  private

  def comment_params
    params[:comment].permit!
  end
end
