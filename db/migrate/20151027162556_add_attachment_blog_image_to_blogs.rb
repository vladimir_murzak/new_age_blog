class AddAttachmentBlogImageToBlogs < ActiveRecord::Migration
  def self.up
    remove_attachment :blogs, :blog_image
    change_table :blogs do |t|
      t.attachment :blog_image
    end
  end

  def self.down
    add_attachment :blogs, :blog_image
    remove_attachment :blogs, :blog_image
  end
end
