class ChangePostsRelationToBlogs < ActiveRecord::Migration
  def up
    remove_column :posts, :user_id
    add_column :posts, :blog_id, :integer
  end

  def down
    remove_column :posts, :blog_id
    add_column :posts, :user_id, :integer
  end
end
